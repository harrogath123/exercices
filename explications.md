# 1. Logging.

L'import Logging sert à définir des classes et des fonctions qui implémentent un système de journalisation dévènements pour les applications et les bibliothèques.

Tous les modules en API peuvent intégrer des messages provenant de modules tiers.

# 1.1 Asyncio.

L'import Asyncio permet la connexion à des bibliothèques de base de données, des utilitaires réseaux, des serveurs web performants etc.

# 1.2 Aiohttp.

L'import Aiohttp sert à écrire du code plus efficacement en lien avec l'import Aysncio et de tirer pleinement parti des ressources disponibles et d'obtenir des meilleures performances.

# 1.3 Request.

La bibliothèque Request permet de fournir une fonctionnalité simple pour traiter les requêtes/ réponses HTTP dans notre application web.

Request: la bibliothèque des requêtes[ python](https://www.tresfacile.net/la-bibliotheque-python-requests/#) propose des méthodes faciles à utiliser pour gérer les requêtes HTTP. Passer des paramètres et gérer le type de requête comme  **GET, POST, PUT, DELETE** , etc. est très facile.

**Timeouts:** les délais d'attente peuvent être facilement ajoutés à l'URL que vous demandez à l'aide de la bibliothèque de requêtes[ python](https://www.tresfacile.net/la-bibliotheque-python-requests/#).

Response: vous pouvez obtenir la réponse dans le format dont vous avez besoin et ceux pris en charge sont le format **texte** , la réponse binaire , la réponse json et la réponse  brute .

Headers:la bibliothèque vous permet de lire, de mettre à jour ou d'envoyer de nouveaux en-têtes selon vos besoins.

# 1.4 A quoi sert cette application?

Cette application sert à récupérer des données à partir d'une API de contrôle de gestion (VCS). Ce code permet de simplifier le processus de récupération et de traitement des données.

# 1.5 Les avantages de cette méthode.

Les avantages de l'asynchronisme dans cette application sont: de meilleures performances, de mieux utiliser ses ressources, de réduire le délai d'attente et d'augmenter la capacité de traitement.
